#include <iostream>     // ���������� ����-�����
using namespace std;    // ���� �� ����������� std;

class Animal {          // ������� ����� Animal
public:                 //������ ��� ���������
   virtual void Voice() {   // ������� �����, ��� � ������� virtual ������� ��� ����� ����� ���� ������������� � ������� �����������

    }
};
class Dog : public Animal {     //Dog ����������� �� Animal

public: 
    void Voice() override {         // ������� ����� Voice � � ������� override �������������� ��� ����� ��������
        cout << "Woof!" << endl;
    }
};

class Cat : public Animal {     //Cat ���������� �� Animal

public:
    void Voice() override {     // ������� ����� Voice � � ������� override �������������� ��� ����� ��������
        cout << "Meow!" << endl;
        
    }

};

class Cow : public Animal {     //Cow ����������� �� Animal

public:
    void Voice() override {     // ������� ����� Voice � � ������� override �������������� ��� ����� ��������
        cout << "Mooo!" << endl;
    }

};

int main()
{
    cout << "Animal voices below:   " << endl;      

    Animal* animalVoices[3];        //������� ������ ���������� ���� Animal � ������� (��������� � ������� *)
    animalVoices[0] = new Dog();    //������� ������
    animalVoices[1] = new Cat();    //������� ������
    animalVoices[2] = new Cow();    //������� ������

    for (Animal* couning : animalVoices)    //������� ���� ��� � ������� couning ���������� �������
        couning->Voice();                   // ��������� �� ����� Voice � ������� �� �����
}
